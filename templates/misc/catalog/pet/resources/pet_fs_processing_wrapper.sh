#!/bin/bash -x


#wrapper script to either launch PIB or FDG processing based on the tracer parameter

paramsfile=$1
tracer=$2

if [ "$tracer" = "PIB" ]; then
echo "Launching pib_fs_proc $paramsfile"
pib_fs_proc $paramsfile
else
echo "Launching fdg_fs_proc $paramsfile"
fdg_fs_proc $paramsfile
fi

exit 0;