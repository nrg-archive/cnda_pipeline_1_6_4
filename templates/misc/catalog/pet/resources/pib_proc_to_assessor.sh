#!/bin/bash 

# if too few arguments, print version number and exit
if test -z "$3"; then
  echo 1.0
  exit
fi

#exit on failure
set -e
set -x

# input parameters

project=$1
session=$2
sessLabel=$3
fsid=$4
petid=$5
isstat=$6
tracer=$7
t1_root=$8

declare -a out_files

if [ "$tracer" = "PIB" ]; then
proc_dir=pib_proc
else
isstat=1
proc_dir=fdg_proc
fi

if [ "$isstat" = "0" ]; then
a_error=`tail -2 ${proc_dir}/${3}_a_t4_resolve.log | grep "ERROR =" | awk '{split ($0,a,/= /); print a[2];}'`
b_error=`tail -2 ${proc_dir}/${3}_b_t4_resolve.log | grep "ERROR =" | awk '{split ($0,a,/= /); print a[2];}'`
out_files=(${proc_dir}/${sessLabel}_ROIPIB.txt ${proc_dir}/${sessLabel}_ROIPIBLR.txt)
a_error_xml_str="<xnat:field name=\"ERROR_A_T4_RESOLVE\">$a_error</xnat:field>"
b_error_xml_str="<xnat:field name=\"ERROR_B_T4_RESOLVE\">$b_error</xnat:field>"
else
if [ "$tracer" = "PIB" ]; then
out_files=(${proc_dir}/${sessLabel}_ROIPIBS.txt ${proc_dir}/${sessLabel}_ROIPIBSLR.txt)
else
out_files=(${proc_dir}/${sessLabel}_ROIPETS.txt ${proc_dir}/${sessLabel}_ROIPETSLR.txt)
fi
a_error_xml_str=""
b_error_xml_str=""
fi

if [ "$isstat" = "0" ]; then
mov_error=`tail -2 ${proc_dir}/${t1_root}001_${tracer}_MR_mov.log | grep "ERROR =" | awk '{split ($0,a,/= /); print a[2];}'`
else
mov_error=`tail -2 ${proc_dir}/${sessLabel}_${tracer}_MR_mov.log | grep "ERROR =" | awk '{split ($0,a,/= /); print a[2];}'`
fi


xml_file=${sessLabel}.xml
temp_file=xnatXML_temp.dat


	touch $xml_file
	touch $temp_file


datestamp=`date '+%Y%m%d%H%M'`

c_error=`tail -2 ${proc_dir}/${sessLabel}_c_t4_resolve.log | grep "ERROR =" | awk '{split ($0,a,/= /); print a[2];}'`


declare -a MEASURES

	  for j in 0 1
	  do
		i=0
		while read line; do
		if [ "$i" -gt "0" ]
		then
			MEASURES=(`echo $line`)
			if [ "$isstat" = "0" ]; then
				echo "<"pet:roi name=\"${MEASURES[0]}\"">" >> $temp_file	
				echo  "<"pet:NVox">"${MEASURES[1]}"<"/pet:NVox">" >> $temp_file
				echo  "<"pet:BP">"${MEASURES[2]}"<"/pet:BP">" >> $temp_file
				echo  "<"pet:R2_BP">"${MEASURES[3]}"<"/pet:R2_BP">" >> $temp_file
				echo  "<"pet:INTC_BP">"${MEASURES[4]}"<"/pet:INTC_BP">" >> $temp_file
				echo  "<"pet:BP_RSF">"${MEASURES[5]}"<"/pet:BP_RSF">" >> $temp_file
				echo  "<"pet:R2_RSF">"${MEASURES[6]}"<"/pet:R2_RSF">" >> $temp_file
				echo  "<"pet:INTC_RSF">"${MEASURES[7]}"<"/pet:INTC_RSF">" >> $temp_file
				echo  "<"pet:BP_PVC2C">"${MEASURES[8]}"<"/pet:BP_PVC2C">" >> $temp_file
				echo  "<"pet:R2_PVC2C">"${MEASURES[9]}"<"/pet:R2_PVC2C">" >> $temp_file
				echo  "<"pet:INTC_PVC2C">"${MEASURES[10]}"<"/pet:INTC_PVC2C">" >> $temp_file
				echo  "<"pet:SUVR">"${MEASURES[11]}"<"/pet:SUVR">" >> $temp_file
				echo  "<"pet:SUVR_RSF">"${MEASURES[12]}"<"/pet:SUVR_RSF">" >> $temp_file
				echo  "<"pet:SUVR_PVC2C">"${MEASURES[13]}"<"/pet:SUVR_PVC2C">" >> $temp_file
				echo "<"/pet:roi">" >> $temp_file
			else
				echo "<"pet:roi name=\"${MEASURES[0]}\"">" >> $temp_file	
				echo  "<"pet:NVox">"${MEASURES[1]}"<"/pet:NVox">" >> $temp_file
				echo  "<"pet:SUVR">"${MEASURES[2]}"<"/pet:SUVR">" >> $temp_file
				echo  "<"pet:SUVR_RSF">"${MEASURES[3]}"<"/pet:SUVR_RSF">" >> $temp_file
				echo  "<"pet:SUVR_PVC2C">"${MEASURES[4]}"<"/pet:SUVR_PVC2C">" >> $temp_file
				echo "<"/pet:roi">" >> $temp_file
			fi
		fi
		let "i += 1"
		done < ${out_files[$j]}	
	  done


cat << EOF > $xml_file
<?xml version="1.0" encoding="UTF-8"?>
<pet:FSPETTimeCourse ID="$petid" project="$project" label="${sessLabel}_PETTIMECOURSE_$datestamp"  xmlns:xnat="http://nrg.wustl.edu/xnat" xmlns:pet="http://nrg.wustl.edu/pet" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/xnat https://cnda.wustl.edu/schemas/xnat/xnat.xsd http://nrg.wustl.edu/pet https://cnda.wustl.edu/schemas/pet/pet.xsd">
	<xnat:date>`date "+%Y-%m-%d"`</xnat:date>
	<xnat:fields>
		$a_error_xml_str
		$b_error_xml_str
		<xnat:field name="ERROR_C_T4_RESOLVE">$c_error</xnat:field>
		<xnat:field name="ERROR_MOV_LOG">$mov_error</xnat:field>
		<xnat:field name="ISSTATIC">$isstat</xnat:field>
		<xnat:field name="TRACER">$tracer</xnat:field>
		<xnat:field name="FreeSurfer_ID">$fsid</xnat:field>
	</xnat:fields>	
	<xnat:imageSession_ID>$session</xnat:imageSession_ID>
	<pet:rois>
EOF

cat $temp_file >> $xml_file
echo "<"/pet:rois">" >> $xml_file
echo "<"/pet:FSPETTimeCourse">"  >> $xml_file
\rm -f $temp_file
exit 0

