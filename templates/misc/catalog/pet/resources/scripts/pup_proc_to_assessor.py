#!/usr/bin/python

from lxml.builder import ElementMaker
from lxml.etree import tostring as xmltostring
import datetime as dt
import sys, json, argparse

versionNumber='8'
dateString='20140716'
author='flavin'
progName=sys.argv[0].split('/')[-1]
idstring = '$Id: %s,v %s %s %s Exp $'%(progName,versionNumber,dateString,author)

nsdict = {'xnat':'http://nrg.wustl.edu/xnat',
          'xsi':'http://www.w3.org/2001/XMLSchema-instance',
          'pup':'http://nrg.wustl.edu/pup'}
def ns(namespace,tag):
    return "{%s}%s"%(nsdict[namespace],tag)

def schemaLoc(namespace):
    return "{0} https://cnda.wustl.edu/schemas/{1}/{1}.xsd".format(nsdict[namespace],namespace)

def main():
    #######################################################
    # PARSE INPUT ARGS
    parser = argparse.ArgumentParser(description='Generate XML assessor file from PUP output logs')
    parser.add_argument('-v', '--version',
                        help='Print version number and exit',
                        action='version',
                        version=versionNumber)
    parser.add_argument('--idstring',
                        help='Print id string and exit',
                        action='version',
                        version=idstring)
    parser.add_argument('-t','--template',
                        nargs='?', const=None, default=None,
                        help='Name of template used for template processing (optional)')
    parser.add_argument('-f','--freesurferid',
                        nargs='?', const=None, default=None,
                        help='FreeSurfer assessor ID used for FreeSurfer processing (optional)')
    parser.add_argument('-m','--mrid',
                        nargs='?', const=None, default=None,
                        help='MR session ID used for FreeSurfer or manual processing (optional)')
    parser.add_argument('-P','--project',
                        help='Project')
    parser.add_argument('-S','--sessionId',
                        help='PET session\'s unique ID')
    parser.add_argument('-I','--assessorId',
                        help='PUP assessor ID')
    parser.add_argument('-L','--assessorLabel',
                        help='PUP assessor label')
    parser.add_argument('-T','--procType',
                        help='Type of PUP processing (Manual, Template, or FreeSurfer)')
    parser.add_argument('-M','--model',
                        help='Model used for processing (logan or ...)')
    parser.add_argument('-R','--tracer',
                        help='PET tracer')
    parser.add_argument('-D','--datestamp',
                        help='Datestamp of PUP processing')
    parser.add_argument('-V','--suvr',
                        help='Did SUVR processing?')
    parser.add_argument('-e','--errorFile',
                        help='JSON file containing motion correction and registration errors')
    parser.add_argument('-s','--statsFile',
                        help='JSON file containing ROI names and statistics')
    parser.add_argument('-o','--XMLFilePath',
                        help='Path to XML assessor file (output)')
    args=parser.parse_args()

    sessionId = args.sessionId

    datestamp = args.datestamp
    if len(datestamp) >= 8:
        date = '{}-{}-{}'.format(datestamp[0:4],datestamp[4:6],datestamp[6:8])
    else:
        date = dt.date.today().isoformat()

    assessorElementsDict = {'procType':args.procType, 'model':args.model, 'tracer':args.tracer, 'suvrFlag':args.suvr}
    if args.template:
        assessorElementsDict['templateType']=args.template
    if args.freesurferid:
        assessorElementsDict['FSId']=args.freesurferid
    if args.mrid:
        assessorElementsDict['MRId']=args.mrid

    assessorTitleAttributesDict = {'ID':args.assessorId, 'project':args.project, 'label':args.assessorLabel,
                ns('xsi','schemaLocation'):' '.join(schemaLoc(namespace) for namespace in ('xnat','pup'))}
    #######################################################


    #######################################################
    # READ ROI STAT JSON FILE
    # JSON (and resulting dict) will be of the form {ROIName:{StatTitle:StatNumberValue,...},...}
    with open(args.statsFile,'r') as f:
        statsDict = json.loads(f.read())
    #######################################################

    #######################################################
    # READ ERROR JSON FILE
    # Must contain list of motion correction errors
    # May also contain PET registration error
    with open(args.errorFile,'r') as f:
        errorDict = json.loads(f.read())

    if 'moco_error_list' in errorDict and errorDict['moco_error_list']:
        assessorElementsDict['mocoError'] = str(max(errorDict['moco_error_list']))
    # else:
    #    sys.exit('%s ERROR: No/Empty motion correction error list in %s'%(progName,args.errorFile))

    if 'registration_error' in errorDict:
        assessorElementsDict['regError'] = str(errorDict['registration_error'])
    #######################################################

    #######################################################
    # BUILD ASSESSOR XML
    # Building XML using lxml ElementMaker.
    # For documentation, see http://lxml.de/tutorial.html#the-e-factory
    E = ElementMaker(namespace=nsdict['pup'],nsmap=nsdict)
    assessorXML = E('PUPTimeCourse', assessorTitleAttributesDict,
        E(ns('xnat','date'),date),
        # E(ns('xnat','fields'),
        #     *[E(ns('xnat','field'),{'name':fieldName},fieldVal) for fieldName,fieldVal in fields.iteritems()]
        #     ),
        E(ns('xnat','imageSession_ID'),sessionId),
        E('rois',
            *[E('roi', {'name':roiName},
                *[E(statTitle,stat) for statTitle,stat in roiDict.iteritems()]
                ) for roiName,roiDict in statsDict.iteritems()]
            ),
        *[E(elName,elValue) for elName,elValue in assessorElementsDict.iteritems()]
        )

    print 'Writing assessor XML to %s'%args.XMLFilePath
    # print(xmltostring(assessorXML, pretty_print=True))
    with open(args.XMLFilePath,'w') as f:
        f.write(xmltostring(assessorXML, pretty_print=True, encoding='UTF-8', xml_declaration=True))


if __name__ == '__main__':
    print idstring
    main()
