#!/bin/csh -x
set echo 
set name = $argv[1]
set archivedir = $argv[2]
set project = $argv[3]
set xnat_id = $argv[4]

source $SCRIPTS_HOME/fiv_setup.csh

set wrkdir = $archivedir

if (! -e QC) mkdir QC

@ i = 5
set irun = ()

while ($i <= ${#argv})
	set irun = ($irun $argv[$i])
	@ i++
end

@ nirun = ${#irun}
if ($nirun < 1) exit 1


                         ###creates mov graph######
@ j = 1
while ($j <= $nirun)
set i = $irun[$j]
pushd $wrkdir/movement   #####(first change)
set fname = $name"_b"$i"_faln_dbnd_xr3d"
set output = $name"_b"$i"_faln_dbnd_xr3d_movement"
set fname1 = $wrkdir/bold$i/$name"_b"$i"_faln_dbnd_r3d_avg"
set output1 = $name"_b"$i"_faln_dbnd_r3d_avg_hist"
set z = `wc $fname.rdat | sed 's/ *\([0-9]*\).*/\1/'`
gnuplot << EOF
set terminal postscript landscape color solid 20;20
set key outside top
set output "$1.eps"
set style line 4 lt 1 lw 2
set style line 5 lt 2 lw 2
set style line 6 lt 3 lw 2
set xrange [0:$z]
set yrange [-3:3]
set xlabel "Frame"
set ylabel "mm/deg"

plot "$fname.rdat" using 1:2 title 'X (mm)' with lines, \
"$fname.rdat" using 1:3 title 'Y (mm)' with lines, \
"$fname.rdat" using 1:4 title 'Z (mm)' with lines, \
"$fname.rdat" using 1:5 title 'X (deg)' with lines ls 4, \
"$fname.rdat" using 1:6 title 'Y (deg)' with lines ls 5, \
"$fname.rdat" using 1:7 title 'Z (deg)' with lines ls 6, \
"$fname.rdat" using 1:8 title 'scale' with lines
EOF
 
cat $1.eps | sed "\
s#\(/LT1 .*\) 0 1 0 DL#\1 0 0.5 0 DL#\
s#\(/LT3 .*\) 1 0 1 DL#\1 1 0 0 DL#\
s#\(/LT4 .*\) 0 1 1 DL#\1 0 0.5 0 DL#\
s#\(/LT5 .*\) 1 1 0 DL#\1 0 0 1 DL#\
s#\(/LT8 .*\) 0.5 0.5 0.5 DL#\1 1 0.3 0 DL#\
" >! out.eps
convert out.eps test.jpg
jpegtran -rot 90 test.jpg > test2.jpg

convert test2.jpg $output.gif

convert -thumbnail 300 $output.gif $output"_thumb".gif
/bin/rm -f *eps *jpg
echo $i
gnuplot << EOF

set terminal postscript landscape color solid 20;20
set output "$output1.eps"
set style line 1 lt 1 lw 3
set autoscale
set xlabel "Bins"
set ylabel "Intensity"

  #creates Histogram####
 

plot "$fname1.hist" notitle with lines ls 1 
EOF
convert $output1.eps test.jpg
jpegtran -rot 90 test.jpg > test2.jpg
convert test2.jpg $output1.gif
convert -thumbnail 300 $output1.gif $output1"_thumb".gif
/bin/rm -f *jpg *eps
/bin/mv  *gif ../QC
popd

                     #####creates txt file for mov data#########
pushd $wrkdir/movement
tail -1 $fname".rdat" | awk '{print $7}' >! mov{$i}.txt
tail -1 $fname".ddat" | awk '{print $7}' >! ddat_mov{$i}.txt
/bin/mv mov{$i}.txt ../QC
/bin/mv ddat_mov{$i}.txt ../QC
popd


@ j++
end


@ j = 1
while ($j <= $nirun)
set i = $irun[$j]
pushd $wrkdir/bold$i
set sdfile = $name"_b"$i"_faln_dbnd_xr3d_atl"
@ k = `awk '$3 == "[4]" {print $NF}' $sdfile".4dfp.ifh"`
@ l = `echo $k - 1 | bc -l`
set format = "1(1x$l+)"
$RELEASE/var_4dfp -s -f$format $sdfile
ifh2hdr -r20          $sdfile"_sd1"
popd
@ j++
end


pushd QC


touch $$.lst
@ j = 1
while ($j <= $nirun)
set i = $irun[$j]
set file = $archivedir/bold$i/$name"_b"$i"_faln_dbnd_xr3d_atl.4dfp"
echo $file >> $$.lst
@ j++
end
$RELEASE/conc_4dfp $name"_faln_dbnd_xr3d_atl" -l$$.lst -w
/bin/rm $$.lst
$RELEASE/compute_defined_4dfp "$name"_faln_dbnd_xr3d_atl.conc

popd

pushd QC

@ j = 1
while ($j <= $nirun)
set i = $irun[$j]

set sdfile1 = $archivedir/bold$i/$name"_b"$i"_faln_dbnd_xr3d_atl_sd1"
set mask = $name"_faln_dbnd_xr3d_atl_dfnd"
$RELEASE/qnt_4dfp $sdfile1 $mask | awk '/Mean=/{print $NF}' >! sd_bold$i.txt
#/bin/mv sd_bold$i.txt ../QC
@ j++
end
popd

                ########### Below to create snapshots####3
set paramsfile = `ls -t $archivedir/${name}.params | awk 'NR=='1' {print $1}'`
set atl_session = `grep  day1_patid $paramsfile | awk '{split($2,a,"="); print a[2]}'`

echo params file $paramsfile atl_session is $atl_session

if ($atl_session == "") then
	pushd atlas

	set name1 = `/bin/ls ${name}_mpr?_to*log`
	echo $name1

	tail -1 $name1 | awk '/eta,q/ {print $2}' >! eta_mpr_to_target.txt
	if (-e ${name}"_mpr1_epi2t2w2mpr2atl2_4dfp.log") then
		gawk -f $RELEASE/parse_epi2i2w2mpr2atl2_log.awk "${name}"_mpr1_epi2t2w2mpr2atl2_4dfp.log | awk '/eta =/ {print $3}' >! eta_temp.txt
		awk 'NR == 1 {print}' eta_temp.txt >! eta_t2_to_mpr.txt
		awk 'NR == 2 {print}' eta_temp.txt >!  eta_anatave_to_t2.txt
	else
		echo NA >! eta_t2_to_mpr.txt
		echo NA >! eta_anatave_to_t2.txt

	endif
	/bin/rm -f eta_temp.txt
	/bin/mv eta_mpr_to_target.txt eta_t2_to_mpr.txt eta_anatave_to_t2.txt ../QC
	set fname = `/bin/ls ${name}_anat_ave_t88_333.4dfp.img`
	set fname3 = `echo $fname | sed 's/\.4dfp.img//'`
	set fname2 = `/bin/ls ${name}_mpr_n?_333_t88.4dfp.img`

	set fname4 = `echo $fname2 | sed 's/\.4dfp.img//'`
	fiv -s $fname2 -m fmri -snap $name"_mpr".gif
	fiv -s $fname -m fmri -snap $fname3.gif
	convert -sample 40%x40% $name"_mpr".gif $name"_mpr_thumb".gif
	convert -sample 40%x40% $fname3.gif $fname3"_thumb".gif
	/bin/mv *gif ../QC
	popd

else
	pushd $archivedir/atlas
	set atl_session_path = `grep  day1_path $paramsfile | awk '{split($2,a,"="); print a[2]}'`

	set name1 = `/bin/ls $atl_session_path/${atl_session}_mpr?_to*log`
	echo $name1

	tail -1 $name1 | awk '/eta,q/ {print $2}' >! eta_mpr_to_target.txt
	if (-e $atl_session_path/${atl_session}"_mpr1_epi2t2w2mpr2atl2_4dfp.log") then
		gawk -f $RELEASE/parse_epi2i2w2mpr2atl2_log.awk $atl_session_path/"${atl_session}"_mpr1_epi2t2w2mpr2atl2_4dfp.log | awk '/eta =/ {print $3}' >! eta_temp.txt
		awk 'NR == 1 {print}' eta_temp.txt >! eta_t2_to_mpr.txt
		awk 'NR == 2 {print}' eta_temp.txt >! eta_anatave_to_t2.txt
	else
		echo NA >! eta_t2_to_mpr.txt
		echo NA >! eta_anatave_to_t2.txt

	endif
	/bin/rm -f eta_temp.txt
	/bin/mv eta_mpr_to_target.txt eta_t2_to_mpr.txt eta_anatave_to_t2.txt ../QC

	set fname = `/bin/ls $atl_session_path/${atl_session}_anat_ave_t88_333.4dfp.img`
	set fname3 = `echo $fname | sed 's/\.4dfp.img//'`
	set fname2 = `/bin/ls $atl_session_path/${atl_session}_mpr_n?_333_t88.4dfp.img`
	set fname4 = `echo $fname2 | sed 's/\.4dfp.img//'`
	fiv -s $fname2 -m fmri -snap `pwd`/$name"_mpr".gif
	fiv -s $fname -m fmri -snap `pwd`/$name"_anat_ave_t88_333".gif
	convert -sample 40%x40% $name"_mpr".gif $name"_mpr_thumb".gif
	convert -sample 40%x40% $name"_anat_ave_t88_333".gif $name"_anat_ave_t88_333_thumb".gif
	/bin/mv *gif ../QC

	popd
endif
exit 0























